<?php


add_action('wp_ajax_nopriv_search', 'search_ajax');
add_action('wp_ajax_search', 'search_ajax');

function search_ajax()
{
    $recipes = $_POST['recipes'];

    $args = [
        'post_type' => 'recipes',
        's' => $recipes,
        'posts_per_page' => '-1'
    ];

    $query = new \WP_Query($args);
    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
?>


<article id="post-510"
    class="elementor-post elementor-grid-item ecs-post-loop post-510 recipes type-recipes status-publish has-post-thumbnail hentry"
    style="position:relative">
    <a style="position: absolute;top:0;left:0;right:0;bottom:0;z-index:10" href="<?= get_the_permalink() ?>"></a>
    <div class="ecs-link-wrapper" data-href="<?= get_the_permalink() ?>">
        <style id="elementor-post-dynamic-338">
        .elementor-338 .elementor-element.elementor-element-49a3fab:not(.elementor-motion-effects-element-type-background)>.elementor-column-wrap,
        .elementor-338 .elementor-element.elementor-element-49a3fab>.elementor-column-wrap>.elementor-motion-effects-container>.elementor-motion-effects-layer {
            background-image: url("https://solpak.t4d.space/wp-content/uploads/2020/11/4_Dzwonki-1-1.jpg");
        }
        </style>
        <div data-elementor-type="loop" data-elementor-id="338"
            class="elementor elementor-338 elementor-location-archive post-510 recipes type-recipes status-publish has-post-thumbnail hentry"
            data-elementor-settings="[]">
            <div class="elementor-section-wrap">
                <section
                    class="elementor-section elementor-top-section elementor-element elementor-element-bdece1d elementor-section-full_width elementor-section-height-default elementor-section-height-default"
                    data-id="bdece1d" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-4fcf1a9"
                                data-id="4fcf1a9" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-7170fec elementor-widget__width-initial elementor-absolute elementor-widget elementor-widget-post-info"
                                            data-id="7170fec" data-element_type="widget"
                                            data-settings="{&quot;_position&quot;:&quot;absolute&quot;}"
                                            data-widget_type="post-info.default">
                                            <div class="elementor-widget-container">
                                                <ul
                                                    class="elementor-inline-items elementor-icon-list-items elementor-post-info">
                                                    <li
                                                        class="elementor-icon-list-item elementor-repeater-item-e0280a8 elementor-inline-item">
                                                        <span
                                                            class="elementor-icon-list-text elementor-post-info__item elementor-post-info__item--type-custom">
                                                            <?php

                                                                        $tags = get_the_tags(); //taxonomy=post_tag

                                                                        if ($tags) :
                                                                            $i = 0;
                                                                            foreach ($tags as $tag) :
                                                                        ?>


                                                            <a href="<?= get_tag_link($tag->term_id) ?>">
                                                                <?= $i > 0 ? ', ' : null ?>
                                                                <?php echo esc_html($tag->name); ?>

                                                            </a>


                                                            <?php $i++;
                                                                            endforeach; ?>

                                                </ul>
                                                <?php endif; ?>
                                                </span>
                                                </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="elementor-section elementor-inner-section elementor-element elementor-element-59cf835 elementor-section-full_width elementor-section-height-default elementor-section-height-default"
                                            data-id="59cf835" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">

                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-49a3fab"
                                                        data-id="49a3fab" data-element_type="column"
                                                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                        <div class="elementor-column-wrap"
                                                            style="background-image:url(<?= get_the_post_thumbnail_url() ?>)">
                                                            <div class="elementor-widget-wrap">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-section elementor-inner-section elementor-element elementor-element-2718bc4 elementor-section-full_width elementor-section-height-default elementor-section-height-default"
                                            data-id="2718bc4" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-270596c"
                                                        data-id="270596c" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-5c2ffa6 elementor-widget elementor-widget-post-info"
                                                                    data-id="5c2ffa6" data-element_type="widget"
                                                                    data-widget_type="post-info.default">
                                                                    <div class="elementor-widget-container">
                                                                        <ul
                                                                            class="elementor-inline-items elementor-icon-list-items elementor-post-info">
                                                                            <li
                                                                                class="elementor-icon-list-item elementor-repeater-item-adedc5c elementor-inline-item">
                                                                                <span class="elementor-icon-list-icon">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                        width="20" height="20"
                                                                                        viewBox="0 0 20 20">
                                                                                        <g id="time"
                                                                                            transform="translate(-2.25 -2.25)">
                                                                                            <path id="Path_24"
                                                                                                data-name="Path 24"
                                                                                                d="M12.25,22.25a10,10,0,1,1,10-10A10,10,0,0,1,12.25,22.25Zm0-18.571a8.571,8.571,0,1,0,8.571,8.571A8.571,8.571,0,0,0,12.25,3.679Z"
                                                                                                transform="translate(0 0)"
                                                                                                fill="#bd5c05"></path>
                                                                                            <path id="Path_25"
                                                                                                data-name="Path 25"
                                                                                                d="M20.785,18.365l-3.91-3.91V7.875h1.4v6l3.5,3.5Z"
                                                                                                transform="translate(-5.275 -1.967)"
                                                                                                fill="#bd5c05"></path>
                                                                                        </g>
                                                                                    </svg> </span>
                                                                                <span
                                                                                    class="elementor-icon-list-text elementor-post-info__item elementor-post-info__item--type-custom">
                                                                                    <?= get_field('recipe_time'); ?>
                                                                                </span>
                                                                            </li>
                                                                            <li
                                                                                class="elementor-icon-list-item elementor-repeater-item-baf9198 elementor-inline-item">
                                                                                <span class="elementor-icon-list-icon">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                        width="20.036" height="18.605"
                                                                                        viewBox="0 0 20.036 18.605">
                                                                                        <g id="skill-level"
                                                                                            transform="translate(-2.25 -4.5)">
                                                                                            <path id="Path_26"
                                                                                                data-name="Path 26"
                                                                                                d="M30.662,23.1H24.75V4.5h5.912Zm-4.434-1.431h2.956V5.931H26.228Z"
                                                                                                transform="translate(-8.376)"
                                                                                                fill="#bd5c05"></path>
                                                                                            <path id="Path_27"
                                                                                                data-name="Path 27"
                                                                                                d="M19.412,26.8H13.5V13.5h5.912Zm-4.434-1.478h2.956V14.978H14.978Z"
                                                                                                transform="translate(-4.188 -3.698)"
                                                                                                fill="#bd5c05"></path>
                                                                                            <path id="Path_28"
                                                                                                data-name="Path 28"
                                                                                                d="M8.162,29.118H2.25V20.25H8.162ZM3.728,27.64H6.684V21.728H3.728Z"
                                                                                                transform="translate(0 -6.014)"
                                                                                                fill="#bd5c05"></path>
                                                                                        </g>
                                                                                    </svg> </span>
                                                                                <span
                                                                                    class="elementor-icon-list-text elementor-post-info__item elementor-post-info__item--type-custom">
                                                                                    <?= get_field('recipe_level'); ?></span>
                                                                            </li>
                                                                            <li
                                                                                class="elementor-icon-list-item elementor-repeater-item-a12ea20 elementor-inline-item">
                                                                                <span class="elementor-icon-list-icon">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                        width="25.401" height="19.051"
                                                                                        viewBox="0 0 25.401 19.051">
                                                                                        <g id="people"
                                                                                            transform="translate(0 -4.5)">
                                                                                            <path id="Path_31"
                                                                                                data-name="Path 31"
                                                                                                d="M23.814,23.551A1.555,1.555,0,0,0,25.4,21.963c0-1.588-1.588-6.35-7.938-6.35s-7.938,4.763-7.938,6.35a1.555,1.555,0,0,0,1.588,1.588Zm-12.693-1.5v0Zm.028-.089H23.779l.023,0,.013,0a4.889,4.889,0,0,0-1.207-2.731A6.357,6.357,0,0,0,17.463,17.2a6.362,6.362,0,0,0-5.144,2.026,4.875,4.875,0,0,0-1.207,2.731l.035.006Zm12.658.089v0Zm-6.343-9.614a3.175,3.175,0,1,0-3.175-3.175A3.175,3.175,0,0,0,17.463,12.438Zm4.763-3.175A4.763,4.763,0,1,1,17.463,4.5a4.763,4.763,0,0,1,4.763,4.763ZM11.011,16.058a9.335,9.335,0,0,0-1.953-.392q-.559-.053-1.12-.052C1.588,15.613,0,20.376,0,21.963a1.4,1.4,0,0,0,1.588,1.588H8.281a3.553,3.553,0,0,1-.343-1.588,7.283,7.283,0,0,1,1.731-4.61A8.428,8.428,0,0,1,11.011,16.058ZM7.811,17.2a6.314,6.314,0,0,0-5.017,2.026,4.858,4.858,0,0,0-1.207,2.737H6.35A8.737,8.737,0,0,1,7.811,17.2Zm-5.43-7.144a4.763,4.763,0,1,1,4.763,4.763A4.763,4.763,0,0,1,2.381,10.057ZM7.144,6.881a3.175,3.175,0,1,0,3.175,3.175A3.175,3.175,0,0,0,7.144,6.881Z"
                                                                                                transform="translate(0 0)"
                                                                                                fill="#bd5c05"
                                                                                                fill-rule="evenodd">
                                                                                            </path>
                                                                                        </g>
                                                                                    </svg> </span>
                                                                                <span
                                                                                    class="elementor-icon-list-text elementor-post-info__item elementor-post-info__item--type-custom">
                                                                                    <?= get_field('recipe_size'); ?></span>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-8c3fae6 elementor-widget elementor-widget-theme-post-title elementor-page-title elementor-widget-heading"
                                                                    data-id="8c3fae6" data-element_type="widget"
                                                                    data-widget_type="theme-post-title.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h2
                                                                            class="elementor-heading-title elementor-size-default">
                                                                            <?= get_the_title() ?></h2>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</article>











<?php }
        \wp_reset_postdata();
    }

    die();
}