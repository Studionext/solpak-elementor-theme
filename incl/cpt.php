<?php

function t4d_add_cpt_recipes()
{
    register_post_type(
        'recipes',
        array(
            'labels' => array(
                'name' => __('Przepisy', 't4d_theme'),
                'singular_name' => __('Przepis', 't4d_theme'),

            ),
            'public' => true,
            'menu_position' => 8,
            'menu_icon' => 'dashicons-food',
            'has_archive' => true,
            'capability_type' => 'post',
            'hierarchical' => true,
            'rewrite' => array('slug' => 'przepisy'),
            'taxonomies' => array('post_tag'),
            'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
        )
    );
}

add_action('init', 't4d_add_cpt_recipes');
