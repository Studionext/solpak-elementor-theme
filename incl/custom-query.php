<?php

add_action('elementor/query/products_query', function ($query) {
    $query->set('post_type', 'page');
    $query->set('posts_per_page', -1);
    $query->set('post_parent', get_post()->ID);
    $query->set('order', 'ASC');
    $query->set('orderby', 'menu_order');
});
