<?php

function t4d_defer_scripts($tag, $handle)
{

	$handles_to_defer = array(
		'jquery',
		// 'jsPDF',

	);
	if (!in_array($handle, $handles_to_defer)) {
		return $tag;
	}

	return str_replace(' src', ' defer="defer" src', $tag);
}

if (!is_admin()) {
	add_filter('script_loader_tag', 't4d_defer_scripts', 10, 2);
}


function wpb_adding_scripts()
{
	wp_register_script('customJS', get_template_directory_uri() . '/js/custom-js.js', array('jquery'), null, true);
	wp_enqueue_script('customJS');

	wp_enqueue_script('ajax-filter', get_template_directory_uri() . '/js/ajax-filter.js', array('jquery'), null, true);
	wp_localize_script(
		'ajax-filter',
		'wp_ajax',
		array('ajax_url' => admin_url('admin-ajax.php'))
	);

	wp_enqueue_script('ajax-search', get_template_directory_uri() . '/js/ajax-search.js', array('jquery'), null, true);
	wp_localize_script(
		'ajax-search',
		'wp_ajax',
		array('ajax_url' => admin_url('admin-ajax.php'))
	);
}
add_action('wp_enqueue_scripts', 'wpb_adding_scripts', 20, 1);