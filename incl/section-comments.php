<?php


function placeholder_author_email_url_form_fields($fields)
{
    $replace_author = __('Imię*', 'solpak');
    $replace_email = __('Email*', 'solpak');
    $replace_url = __('Facebook (opcjonalnie)', 'solpak');

    $fields['author'] = '<p class="comment-form-author">' . '<label for="author">' . __('Name', 'yourdomain') . '</label> ' . ($req ? '<span class="required">*</span>' : '') .
        '<input id="author" name="author" type="text" placeholder="' . $replace_author . '" value="' . esc_attr($commenter['comment_author']) . '" size="20"' . $aria_req . ' /></p>';

    $fields['email'] = '<p class="comment-form-email"><label for="email">' . __('Email', 'yourdomain') . '</label> ' .
        ($req ? '<span class="required">*</span>' : '') .
        '<input id="email" name="email" type="text" placeholder="' . $replace_email . '" value="' . esc_attr($commenter['comment_author_email']) .
        '" size="30"' . $aria_req . ' /></p>';

    $fields['url'] = '<p class="comment-form-url"><label for="url">' . __('Website', 'yourdomain') . '</label>' .
        '<input id="url" name="url" type="text" placeholder="' . $replace_url . '" value="' . esc_attr($commenter['comment_author_url']) .
        '" size="30" /></p>';

    return $fields;
}
add_filter('comment_form_default_fields', 'placeholder_author_email_url_form_fields');



function placeholder_comment_form_field($fields)
{
    $replace_comment = __('Wpisz komentarz*', 'solpak');

    $fields['comment_field'] = '<p class="comment-form-comment"><label for="comment">' . _x('Comment', 'noun') .
        '</label><textarea id="comment" name="comment" cols="45" rows="8" placeholder="' . $replace_comment . '" aria-required="true"></textarea></p>';

    return $fields;
}
add_filter('comment_form_defaults', 'placeholder_comment_form_field');
