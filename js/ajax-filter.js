(function ($) {

    // $(document).ready(function () {
    //     const levelCheckboxes = document.querySelectorAll('[data-type="level"] input');
    //     const typeCheckboxes = document.querySelectorAll('[data-type="type"] input');

    //     levelCheckboxes.forEach(item => {
    //         let checked = false;
    //         item.addEventListener('click', () => {
    //             levelCheckboxes.forEach(box => box.checked = false);
    //             item.checked = true;
    //         })
    //     })
    //     typeCheckboxes.forEach(item => {

    //         item.addEventListener('click', () => {
    //             typeCheckboxes.forEach(box => box.checked = false);
    //             item.checked = true;

    //         })
    //     })
    // });

    $(document).ready(function () {
        $('.show-filters').on('click', function (e) {
            e.preventDefault();
            const levelCheckboxes = document.querySelectorAll('[data-type="level"] input');
            const typeCheckboxes = document.querySelectorAll('[data-type="type"] input');
            const timeCheckboxes = document.querySelectorAll('[data-type="time"] input');

            let level;
            let type;
            let time

            levelCheckboxes.forEach(checkbox => {
                if (checkbox.checked === true) {
                    level = checkbox.dataset.level;
                }
            })
            typeCheckboxes.forEach(checkbox => {
                if (checkbox.checked === true) {
                    type = checkbox.dataset.type;
                }
            })
            timeCheckboxes.forEach(checkbox => {
                if (checkbox.checked === true) {
                    time = checkbox.dataset.time;
                }
            })

            $.ajax({
                url: wp_ajax.ajax_url,
                data: {
                    action: 'filter',
                    level: level,
                    type: type,
                    time: time
                },
                type: 'post',
                success: function (resault) {
                    $('.recipes-list .elementor-posts-container').html(resault);
                    levelCheckboxes.forEach(chechbox => chechbox.checked = false);
                    typeCheckboxes.forEach(chechbox => chechbox.checked = false);
                    timeCheckboxes.forEach(chechbox => chechbox.checked = false);
                },
                error: function (resault) {
                    console.warn(resault);
                }
            })
        })
        //hide filters
        $('.hide-filters').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: wp_ajax.ajax_url,
                data: {
                    action: 'filter',

                },
                type: 'post',
                success: function (resault) {
                    $('.recipes-list .elementor-posts-container').html(resault);
                },
                error: function (resault) {
                    console.warn(resault);
                }
            })
        })
    })
})(jQuery);