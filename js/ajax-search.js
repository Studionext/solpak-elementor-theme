(function ($) {

    $(document).ready(function () {

        const $contentContainer = $('.recipes-list .elementor-posts-container');

        let typingTimer; //timer identifier
        const doneTypingInterval = 1000; //time in ms, 5 second for example
        const $input = $('.js-name-input');

        //on keyup, start the countdown
        $input.on('keyup', function (e) {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(() => doneTyping(e), doneTypingInterval);
        });

        //on keydown, clear the countdown 
        $input.on('keydown', function () {
            clearTimeout(typingTimer);
        });

        const urlParams = new URLSearchParams(location.search);

        for (const [key, value] of urlParams) {
            if (key === 'search') {

                doneTyping(null, $input.val())


            }
        }

        //user is "finished typing," do something

        function doneTyping(e, value) {
            let recipes;
            if (e === null) {
                recipes = value;
            } else {

                $(this).value = e.target.value;
                recipes = e.target.value;
            }

            $.ajax({
                url: wp_ajax.ajax_url,
                data: {
                    action: 'search',
                    recipes: recipes,

                },
                type: 'post',
                beforeSend: function () {

                    if (!document.querySelector('.loader_wrapper')) {
                        const loaderWrapper = document.createElement('div');
                        loaderWrapper.classList.add('loader_wrapper');
                        const loaderImg = document.createElement('img');
                        loaderImg.src = "https://solpak.t4d.space/wp-content/uploads/2020/11/pszenna_final2.png";
                        loaderImg.classList.add('loader_img');
                        loaderWrapper.appendChild(loaderImg);
                        document.querySelector('.recipes-list .elementor-posts-container').parentElement.appendChild(loaderWrapper);
                        loaderWrapper.classList.add('active');
                    }

                },
                success: function (resault) {

                    $contentContainer.html(resault);

                    $('.elementor-pagination').fadeOut('fast');

                    if (recipes === "") {
                        $('.elementor-pagination').fadeIn('fast');
                    }

                    if (resault === "") {
                        $contentContainer.html("<h2 class='error'>Nie znaleziono takiego przepisu</h2>");
                    }

                },
                complete: function () {

                    if (document.querySelector('.loader_wrapper')) {
                        document.querySelector('.loader_wrapper').remove();
                    }
                },
                error: function (resault) {
                    console.warn(resault);
                    $contentContainer.html("<h2>Wystąpił błąd</h2>");

                }
            })
        }

    })
})(jQuery);