document.addEventListener("DOMContentLoaded", function () {
    const toogleFilters = document.querySelector('.mobile-filters a');
    const filters = document.querySelector('.filters');

    if (document.querySelector('.filters')) {

        toogleFilters.addEventListener('click', () => {
            filters.classList.toggle('active');
            if (filters.classList.contains('active')) {
                toogleFilters.querySelector('.elementor-button-text').textContent = "Schowaj filtry";
            } else {
                toogleFilters.querySelector('.elementor-button-text').textContent = "Pokaż filtry";
            }
            console.log('elko');
        })

    }

    if (document.querySelector('.back')) {


        const getBack = document.querySelector('.back a');
        getBack.addEventListener('click', () => {
            window.history.back();
        })
    }


});