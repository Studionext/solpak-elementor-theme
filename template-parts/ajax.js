(function ($) {
    $(document).ready(function () {
        $('.js-filter-item a').on('click', function (e) {
            e.preventDefault();

            const level = $(this).data('level');

            $.ajax({
                url: wp_ajax.ajax_url,
                data: {
                    action: 'filter',
                    level: level
                },
                type: 'post',
                success: function (resault) {
                    console.log($(this).data('level'));
                    $(this).find(`[data-level]`).prop("checked", true);



                    $('.recipes-list .elementor-posts-container').html(resault);

                },
                error: function (resault) {
                    console.warn(resault);
                }
            })
        })
    })
})(jQuery)