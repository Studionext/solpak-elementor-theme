<?php

namespace Elementor;

class My_Widget_1 extends Widget_Base
{

	public function get_name()
	{
		return 'title-subtitle';
	}

	public function get_title()
	{
		return 'title & sub-title';
	}

	public function get_icon()
	{
		return 'fa fa-font';
	}

	public function get_categories()
	{
		return ['basic'];
	}

	protected function _register_controls()
	{

		// $this->start_controls_section(
		// 	'section_title',
		// 	[
		// 		'label' => __('Content', 'elementor'),
		// 	]
		// );

		// $this->add_control(
		// 	'title',
		// 	[
		// 		'label' => __('Title', 'elementor'),
		// 		'label_block' => true,
		// 		'type' => Controls_Manager::TEXT,
		// 		'placeholder' => __('Enter your title', 'elementor'),
		// 	]
		// );

		// $this->add_control(
		// 	'subtitle',
		// 	[
		// 		'label' => __('Sub-title', 'elementor'),
		// 		'label_block' => true,
		// 		'type' => Controls_Manager::TEXT,
		// 		'placeholder' => __('Enter your sub-title', 'elementor'),
		// 	]
		// );

		// $this->add_control(
		// 	'link',
		// 	[
		// 		'label' => __('Link', 'elementor'),
		// 		'type' => Controls_Manager::URL,
		// 		'placeholder' => __('https://your-link.com', 'elementor'),
		// 		'default' => [
		// 			'url' => '',
		// 		]
		// 	]
		// );

		$this->end_controls_section();
	}

	protected function render()
	{

		$settings = $this->get_settings_for_display();
		$url = $settings['link']['url'];
		// echo  "<a href='$url'><div class='title'>$settings[title]</div> <div class='subtitle'>$settings[subtitle]</div></a>";
?>

		<?php
		if (have_rows('product_list')) : while (have_rows('product_list')) : the_row();
				$img = get_sub_field('image')['sizes']['large'];
				$title = get_sub_field('title');
				$description = get_sub_field('description');
		?>

				<div class="product_item">

					<div class="product_image">

						<img src="<?= $img ?>">

					</div>

					<div class="product_content">

						<div class="elementor-widget-heading">

							<div class="elementor-widget-container">

								<h3 class="elementor-heading-title elementor-size-default">
									<?= $title ?>
								</h3>

							</div>

						</div>


						<?= $description ?>

						<div class="product_images">

							<?php if (have_rows('imgs')) : ?>

								<?php while (have_rows('imgs')) : the_row(); ?>

									<div class="product_images-item">

										<img src="<?= get_sub_field('img')['sizes']['large']; ?>" alt="">

									</div>

								<?php endwhile; ?>
							<?php endif; ?>

						</div>


					</div>

				</div>



		<?php
			endwhile;
		endif;
		?>
<?php

	}

	protected function _content_template()
	{
	}
}
