<?php

namespace Elementor;

class My_Widget_2 extends Widget_Base
{

    public function get_name()
    {
        return 'filters';
    }

    public function get_title()
    {
        return 'filters';
    }

    public function get_icon()
    {
        return 'fa fa-font';
    }

    public function get_categories()
    {
        return ['basic'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('Content', 'elementor'),
            ]
        );

        // $this->add_control(
        //     'title',
        //     [
        //         'label' => __('Title', 'elementor'),
        //         'label_block' => true,
        //         'type' => Controls_Manager::TEXT,
        //         'placeholder' => __('Enter your title', 'elementor'),
        //     ]
        // );

        // $this->add_control(
        //     'subtitle',
        //     [
        //         'label' => __('Sub-title', 'elementor'),
        //         'label_block' => true,
        //         'type' => Controls_Manager::TEXT,
        //         'placeholder' => __('Enter your sub-title', 'elementor'),
        //     ]
        // );

        // $this->add_control(
        //     'link',
        //     [
        //         'label' => __('Link', 'elementor'),
        //         'type' => Controls_Manager::URL,
        //         'placeholder' => __('https://your-link.com', 'elementor'),
        //         'default' => [
        //             'url' => '',
        //         ]
        //     ]
        // );

        $this->end_controls_section();
    }

    protected function render()
    {

        $settings = $this->get_settings_for_display();
        $url = $settings['link']['url'];
        // echo  "<a href='$url'><div class='title'>$settings[title]</div> <div class='subtitle'>$settings[subtitle]</div></a>";
?>


<div class="js-filter">

    <div class="wrapper">


        <div class="group" data-type="type">

            <div class="elementor-widget-heading">

                <h2 class="elementor-heading-title elementor-size-default">

                    Rodzaj dania:

                </h2>

            </div>

            <?php

                    $tags = get_tags('post_tag'); //taxonomy=post_tag

                    if ($tags) : ?>
            <ul>
                <?php
                            foreach ($tags as $tag) :
                                $tag_name = esc_html($tag->name);

                            ?>

                <li class="js-filter-item">



                    <input data-type="<?php echo $tag->term_id ?>" type="radio" name="type"
                        id="<?php echo $tag_name ?>">
                    <label for="<?php echo $tag_name ?>">
                        <?php echo $tag_name ?>

                    </label>


                </li>

                <?php endforeach; ?>

            </ul>
            <?php endif; ?>





        </div>


        <div class="group" data-type="time">

            <div class="elementor-widget-heading">

                <h2 class="elementor-heading-title elementor-size-default">

                    Czas przygotowania

                </h2>

            </div>

            <div class="js-filter-item">


                <input data-time="short" type="radio" name="level" id="short">


                <label for="short">Do 30min</label>


            </div>

            <div class="js-filter-item">


                <input data-time="medium" type="radio" name="level" id="medium">

                <label for="medium">
                    Do 1h

                </label>


            </div>

            <div class="js-filter-item">


                <input data-time="long" type="radio" name="level" id="long">

                <label for="long">
                    Do 2h

                </label>


            </div>



        </div>

        <div class="group" data-type="level">

            <div class="elementor-widget-heading">

                <h2 class="elementor-heading-title elementor-size-default">

                    Stopień trudności:

                </h2>

            </div>

            <div class="js-filter-item">


                <input data-level="easy" type="radio" name="level" id="easy">


                <label for="easy">Łatwe</label>


            </div>

            <div class="js-filter-item">


                <input data-level="medium" type="radio" name="level" id="medium">


                <label for="medium">Średnie</label>


            </div>

            <div class="js-filter-item">


                <input data-level="hard" type="radio" name="level" id="hard">

                <label for="hard">
                    Trudne

                </label>


            </div>


        </div>

        <div class="btn-blue elementor-widget-button">
            <div class="elementor-widget-container">
                <div class="elementor-button-wrapper">
                    <a href="" class="show-filters elementor-button-link elementor-button elementor-size-sm"
                        role="button">
                        <span class="elementor-button-content-wrapper">
                            <span class="elementor-button-text">Zastosuj filtry</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>

        <div class="btn-blue elementor-widget-button">
            <div class="elementor-widget-container">
                <div class="elementor-button-wrapper">
                    <a href="" class="hide-filters elementor-button-link elementor-button elementor-size-sm"
                        role="button">
                        <span class="elementor-button-content-wrapper">
                            <span class="elementor-button-text">Resetuj filtry</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>

    </div>

    <div class="resault">

    </div>

</div>

<?php

    }

    protected function _content_template()
    {
    }
}