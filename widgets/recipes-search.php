<?php

namespace Elementor;

class My_Widget_3 extends Widget_Base
{

    public function get_name()
    {
        return 'search';
    }

    public function get_title()
    {
        return 'search';
    }

    public function get_icon()
    {
        return 'fa fa-search';
    }

    public function get_categories()
    {
        return ['basic'];
    }

    protected function _register_controls()
    {

        $this->start_controls_section(
            'section_title',
            [
                'label' => __('Content', 'elementor'),
            ]
        );

        // $this->add_control(
        //     'title',
        //     [
        //         'label' => __('Title', 'elementor'),
        //         'label_block' => true,
        //         'type' => Controls_Manager::TEXT,
        //         'placeholder' => __('Enter your title', 'elementor'),
        //     ]
        // );

        // $this->add_control(
        //     'subtitle',
        //     [
        //         'label' => __('Sub-title', 'elementor'),
        //         'label_block' => true,
        //         'type' => Controls_Manager::TEXT,
        //         'placeholder' => __('Enter your sub-title', 'elementor'),
        //     ]
        // );

        // $this->add_control(
        //     'link',
        //     [
        //         'label' => __('Link', 'elementor'),
        //         'type' => Controls_Manager::URL,
        //         'placeholder' => __('https://your-link.com', 'elementor'),
        //         'default' => [
        //             'url' => '',
        //         ]
        //     ]
        // );
        $this->end_controls_section();
    }

    protected function render()
    {

        $settings = $this->get_settings_for_display();
        $url = $settings['link']['url'];
        // echo  "<a href='$url'><div class='title'>$settings[title]</div> <div class='subtitle'>$settings[subtitle]</div></a>";
?>


        <div class="js-filter">

            <div class="wrapper">

                <div class="group">

                    <form action="https://solpak.t4d.space/przepisy/" method="get">

                        <input class="js-name-input" type="text" placeholder="Szukaj przepisu..." name="search" value="<?= $_GET['search'] ? $_GET['search'] : '' ?>">
                        <?php if (!is_archive()) : ?>
                            <button type="submit">
                                <img src="https://solpak.t4d.space/wp-content/uploads/2020/12/zoom.png" alt="">
                            </button>

                        <?php else : ?>
                            <button type="text" style="pointer-events:none;">
                                <img src="https://solpak.t4d.space/wp-content/uploads/2020/12/zoom.png" alt="">
                            </button>

                        <?php endif; ?>
                    </form>

                </div>

            </div>

        </div>

<?php

    }

    protected function _content_template()
    {
    }
}
